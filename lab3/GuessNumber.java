import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
        System.out.println(rand);
        
        int attempts = 0 ;

        int guess;
        do{
          System.out.println("Type -1 to quit or give me a guess: " );
          guess=reader.nextInt();
          attempts++;
          if (guess == -1)
           System.out.println("Sorry, the number was " + number);
          else if (guess == number)
           System.out.println("Congratulations you knew", attempts + "times!");
          else if (guess >  number)
           System.out.println("Mine ise less than your guess.");
          else if (guess< number)
           System.out.println("Mine is greater than yours.");
          else 
           System.out.println("Sorry");
       }while (guess !=number && guess != -1 );
   }
}


	

